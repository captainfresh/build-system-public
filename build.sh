#!/bin/bash

#set -e

#${AWS_KEY} ${AWS_SECRET} ${AWS_DEFAULT_REGION}
setupAWS () {
  aws configure set aws_access_key_id "$1"
  aws configure set aws_secret_access_key "$2"
  eval $(aws ecr get-login --region $3 --no-include-email)
}

setupVersion () {
  BITBUCKET_COMMIT_SHORT=$(echo $BITBUCKET_COMMIT | cut -c1-7)
  
  if [ ! -z $BITBUCKET_TAG ]; then
  # Tag based versioning
    VERSION_TAG=$BITBUCKET_TAG
  elif [ ! -z $BITBUCKET_PR_ID ]; then
  # Meta with PR info
    VERSION_TAG="1.0.0-${BITBUCKET_COMMIT_SHORT}.PR-${BITBUCKET_PR_ID}.${BITBUCKET_BUILD_NUMBER}"
  else
    VERSION_TAG="1.0.0-${BITBUCKET_COMMIT_SHORT}.${BITBUCKET_BRANCH}.${BITBUCKET_BUILD_NUMBER}"
  fi
  
  echo $VERSION_TAG
}

createECRRepo() {
  aws ecr describe-repositories --repository-names ${1} || aws ecr create-repository --repository-name ${1}
}

# docker tag
buildDockerImage () {
  docker build -t ${1} .
}

# docker tag
pushDockerImage () {
  docker push ${1}
}

# docker tag
buildAndPushDocker () {
  buildDockerImage  "${1}"
  pushDockerImage "${1}"
}

setupHelm () {
  curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
  chmod 700 get_helm.sh
  ./get_helm.sh $1
  helm plugin install https://github.com/emanuelflp/helm-push.git
}

#APPNAME VERSION IMAGEURL CHART_OUT
buildHelmChart () {
  curl -fsSL -o chart_scaffold.zip https://github.com/ysaakpr/chart-scaffold/archive/refs/heads/main.zip
  unzip chart_scaffold.zip
  chartdir=`unzip -qql chart_scaffold.zip | head -n1 | tr -s ' ' | cut -d' ' -f5-`
  chartdir=${chartdir%?}
  find ${chartdir} -not -path '*/\.*' -type f  -exec sed -i '' -e "s/@@APP_NAME@@/${1}/g" {} +;
  ls -la ${chartdir}
  
  if [ -f "charts/values.yaml" ]; then
    cp charts/values.yaml tempValues.yaml
  else
    cp "${chartdir}/values.yaml" tempValues.yaml
  fi
  
  sed "s|INJECT_IMAGE_URI|${3}|g" < tempValues.yaml > "${chartdir}/values.yaml"
  
  #sed -i  -e "s|INJECT_IMAGE_URI|${3}|g"  "${chartdir}/values.yaml"
  helm package --version ${2} --app-version ${2} ${chartdir} -d ${4}
}

#CF_HELM_REPO CF_HELM_REPO_USER CF_HELM_REPO_PASS PATH
pushHelmChart () {
  helm repo add chartrepo ${1} --username ${2}  --password ${3}
  helm cm-push ${4} chartrepo
}


setupAWS ${AWS_KEY} ${AWS_SECRET} ${AWS_DEFAULT_REGION}
export VERSION_TAG=$(setupVersion)
export IMAGE_NAME="${BITBUCKET_REPO_SLUG}"
export IMAGE_REPO="${AWS_REGISTRY}/${IMAGE_NAME}"
export IMAGE_TAG="${IMAGE_REPO}:${VERSION_TAG}"

apk add --update curl openssl
rm -rf /var/cache/apk/*
createECRRepo ${IMAGE_NAME}
buildAndPushDocker ${IMAGE_TAG}
setupHelm
mkdir chart_out
buildHelmChart ${BITBUCKET_REPO_SLUG} ${VERSION_TAG} ${IMAGE_REPO} ./chart_out
chart=`ls ./chart_out/*.tgz`
echo $chart
pushHelmChart ${CF_HELM_REPO} ${CF_HELM_REPO_USER} ${CF_HELM_REPO_PASS} ${chart}